class Engine{
    constructor(tick_time_in_seconds=5) {
        this.tick_time = tick_time_in_seconds * 1000;
        this.timer = false;
    }

    /**
     * Overwrite this function to do the task in each run
     *
     * @memberof Engine
     */
    run = async() => {
        console.log("Run function is called but not initialized")
    }

    /**
     * Stops the timer of the engine
     *
     * @returns {boolean} Wether the timer was initialized earlier or not
     * @memberof Engine
     */
    stop() {
        if(this.timer) {
            clearInterval(this.timer);
            return true;
        } else return false;
    }

    /**
     * Starts the timer of the engine
     *
     * @memberof Engine
     * @returns {boolean} whether the engine started(true) or it was already started(false) 
     */
    start(tick=this.tick_time) {
        if(this.timer) return false;
        else{
            this.timer = setInterval(()=>{this.run();}, tick);
            return true;
        }
    }
}

module.exports = {
    Engine,
}