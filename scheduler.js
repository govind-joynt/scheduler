const {Engine} = require('./engine/engine');
const { run } = require('googleapis/build/src/apis/run');
const router = require('express').Router();


const engine = new Engine(5);
engine.run = () => {
    console.log("I am in the run function....");
}
// engine.start();

router.get('/start', (req, res)=>{
    engine.start();
    res.send({
        message: "Success starting the scheduler engine"
    });
})


router.get('/stop', (req, res)=>{
    engine.stop();
    res.send({
        message: "Success stopping the scheduler engine"
    });
})

module.exports = {
    router
}