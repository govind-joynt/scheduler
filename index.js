const express = require('express');

app = express();

app.use(require('./scheduler').router);


let port = process.env.PORT || 8000;
app.listen(port, ()=>{
    console.log(`App started on port ${port}`);
})
